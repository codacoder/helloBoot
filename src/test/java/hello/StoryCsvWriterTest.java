//  Copyright (c) 2015 Lance Peterson.  All rights reserved.
//
package hello;

import org.junit.Assert;
import org.junit.Test;

import java.io.StringWriter;

public class StoryCsvWriterTest {
  @Test
  public void write() throws Exception {
    TestStoryBean story = new TestStoryBean();
    StringWriter buf = new StringWriter();
    StoryCsvWriter writer = new StoryCsvWriter(buf);
    writer.writeHeader();
    writer.write(story);

    String result = buf.toString();
    System.out.println(result);

    Assert.assertTrue(result.startsWith("Headline,Body,Modified Date,Created Date,Position,Published,Author Last Name,Author First Name,Author Company,Author Business Title,Author Creation Date,Author Modified Date\n" +
        "Test Headline,\"Story Body: Lorem ipsum dolor sit amet, consectetuer adipiscing elit.\","));
    Assert.assertTrue(result.contains(",15,false,"));
    Assert.assertTrue(result.contains(",Smith,John,Test Company,Test Employee,"));
  }
}
