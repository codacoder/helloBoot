//  Copyright (c) 2015 Lance Peterson.  All rights reserved.
//
package hello;

import org.junit.Assert;
import org.junit.Test;

import java.io.StringWriter;

public class UserCsvWriterTest {
  @Test
  public void write() throws Exception {
    TestUserBean user = new TestUserBean();
    StringWriter buf = new StringWriter();
    UserCsvWriter writer = new UserCsvWriter(buf);
    writer.writeHeader();
    writer.write(user);

    String result = buf.toString();
    System.out.println(result);

    Assert.assertTrue(result.startsWith("Last Name,First Name,Company,Business Title,Creation Date,Modified Date\nSmith,John,Test Company,Test Employee"));
  }
}
