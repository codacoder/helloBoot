//  Copyright (c) 2015 Lance Peterson.  All rights reserved.
//
package hello;

import java.io.IOException;
import java.io.Writer;

public class StoryCsvWriter {
  private Writer writer;

  public StoryCsvWriter(Writer writer) {

    this.writer = writer;
  }

  public void writeHeader() throws IOException {
    writer.write("Headline,Body,Modified Date,Created Date,Position,Published,Author Last Name,Author First Name,Author Company,Author Business Title,Author Creation Date,Author Modified Date\n");
  }

  public void write(TestStoryBean story) throws IOException {
    writer.write(story.getHeadline());
    writer.write(",\"");
    writer.write(story.getBody());
    writer.write("\",");
    writer.write(HelloScripps.DATE_FORMAT.format(story.getModifiedDate()));
    writer.write(",");
    writer.write(HelloScripps.DATE_FORMAT.format(story.getCreatedDate()));
    writer.write(",");
    writer.write(String.valueOf(story.getPosition()));
    writer.write(",");
    writer.write(String.valueOf(story.isPublished()));
    writer.write(",");

    UserCsvWriter userWriter = new UserCsvWriter(writer);
    userWriter.write(story.getAuthor());
  }
}
