//  Copyright (c) 2015 Lance Peterson.  All rights reserved.
//
package hello;

import java.io.IOException;
import java.io.Writer;

public class UserCsvWriter {
  private final Writer writer;

  public UserCsvWriter(Writer writer) {
    this.writer = writer;
  }

  public void writeHeader() throws IOException {
    writer.write("Last Name,First Name,Company,Business Title,Creation Date,Modified Date\n");
  }

  public void write(TestUserBean user) throws IOException {
    writer.write(user.getLastName());
    writer.write(",");
    writer.write(user.getFirstName());
    writer.write(",");
    writer.write(user.getCompany());
    writer.write(",");
    writer.write(user.getBusinessTitle());
    writer.write(",");
    writer.write(HelloScripps.DATE_FORMAT.format(user.getCreationDate()));
    writer.write(",");
    writer.write(HelloScripps.DATE_FORMAT.format(user.getModifiedDate()));
  }
}
