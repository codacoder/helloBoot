package hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class UserController {

  @RequestMapping(
      value = "/user",
      method = RequestMethod.GET)
  public ModelAndView show() {
    ModelAndView mav = new ModelAndView();
    mav.setViewName("user");

    mav.addObject("user", getUser());

    return mav;
  }

  @RequestMapping(
      value = "/user",
      method = RequestMethod.GET,
      produces = {"application/xml", "application/json"})
  public TestUserBean getUser() {
    return new TestUserBean();
  }

  @RequestMapping(
      value = "/user.csv",
      method = RequestMethod.GET)
  @ResponseBody
  public void getUserCSV(HttpServletResponse response) {
    response.setContentType("text/csv; charset=utf-8");
    response.setHeader("Content-Disposition", "attachment; filename=\"user.csv\"");

    TestUserBean user = getUser();

    try {
      UserCsvWriter writer = new UserCsvWriter(response.getWriter());
      writer.writeHeader();
      writer.write(user);
    } catch (IOException e) {
      response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    }
  }

}
