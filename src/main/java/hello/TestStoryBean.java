//  Copyright (c) 2015 Lance Peterson.  All rights reserved.
//
package hello;

import java.util.Date;

public class TestStoryBean
{
  private final String headline = "Test Headline";
  private final TestUserBean author = new TestUserBean();
  private final String body = "Story Body: Lorem ipsum dolor sit amet, consectetuer adipiscing elit.";
  private final Date modifiedDate = new Date();
  private final Date createdDate = new Date();
  private final int position = 15;
  private final boolean published = false;

  public String getHeadline() {
    return headline;
  }

  public TestUserBean getAuthor() {
    return author;
  }

  public String getBody() {
    return body;
  }

  public Date getModifiedDate() {
    return modifiedDate;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public int getPosition() {
    return position;
  }

  public boolean isPublished() {
    return published;
  }
}
