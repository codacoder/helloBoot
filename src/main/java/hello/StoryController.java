package hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class StoryController {

  @RequestMapping(
      value = "/story",
      method = RequestMethod.GET)
  public ModelAndView show() {
    ModelAndView mav = new ModelAndView();
    mav.setViewName("story");

    mav.addObject("story", getStory());

    return mav;
  }

  @RequestMapping(
      value = "/story",
      method = RequestMethod.GET,
      produces = {"application/xml", "application/json"})
  public TestStoryBean getStory() {
    return new TestStoryBean();
  }

  @RequestMapping(
      value = "/story.csv",
      method = RequestMethod.GET)
  @ResponseBody
  public void getUserCSV(HttpServletResponse response) {
    response.setContentType("text/csv; charset=utf-8");
    response.setHeader("Content-Disposition", "attachment; filename=\"story.csv\"");

    TestStoryBean story = getStory();

    try {
      StoryCsvWriter writer = new StoryCsvWriter(response.getWriter());
      writer.writeHeader();
      writer.write(story);
    }
    catch(IOException e) {
      response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    }
  }
}
