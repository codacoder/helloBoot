<!DOCTYPE html>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
  <title>Hello Scripps -- Story</title>
</head>
<body>
  <h1>Story</h1>
  <table>
    <tr>
      <td>Headline</td>
      <td>${story.headline}</td>
    </tr>
    <tr>
      <td>Body</td>
      <td>${story.body}</td>
    </tr>
    <tr>
      <td>Author</td>
      <td><a href="user.html">${story.author.lastName}, ${story.author.firstName}</a></td>
    </tr>
    <tr>
      <td>Modified Date</td>
      <td><fmt:formatDate value="${story.modifiedDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
    </tr>
    <tr>
      <td>Created Date</td>
      <td><fmt:formatDate value="${story.createdDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
    </tr>
    <tr>
      <td>Headline</td>
      <td>${story.position}</td>
    </tr>
    <tr>
      <td>Headline</td>
      <td>${story.published}</td>
    </tr>
  </table>
</body>
</html>
