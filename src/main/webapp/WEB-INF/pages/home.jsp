<!DOCTYPE html>
<html>
<head>
  <title>Hello Scripps</title>
</head>
<body>
  <h1>User</h1>
  <ul>
    <li><a href="user.xml">xml</a></li>
    <li><a href="user.html">web</a></li>
    <li><a href="user.json">json</a></li>
    <li><a href="user.csv">csv</a></li>
  </ul>
  <h1>Story</h1>
  <ul>
    <li><a href="story.xml">xml</a></li>
    <li><a href="story.html">web</a></li>
    <li><a href="story.json">json</a></li>
    <li><a href="story.csv">csv</a></li>
  </ul>
</body>
</html>
