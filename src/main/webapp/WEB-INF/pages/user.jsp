<!DOCTYPE html>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
  <title>Hello Scripps -- User</title>
</head>
<body>
  <h1>User</h1>
  <table>
    <tr>
      <td>Last name</td>
      <td>${user.lastName}</td>
    </tr>
    <tr>
      <td>First name</td>
      <td>${user.firstName}</td>
    </tr>
    <tr>
      <td>Company</td>
      <td>${user.company}</td>
    </tr>
    <tr>
      <td>Business Title</td>
      <td>${user.businessTitle}</td>
    </tr>
    <tr>
      <td>Creation Date</td>
      <td><fmt:formatDate value="${user.creationDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
    </tr>
    <tr>
      <td>Modified Date</td>
      <td><fmt:formatDate value="${user.modifiedDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
    </tr>
  </table>
</body>
</html>
