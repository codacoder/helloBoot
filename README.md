This is a sample project to show what I can do with Spring Boot. Requires Java 8 and git.

To get the source...

    git clone https://gitlab.com/codacoder/helloBoot.git

To run the app's embedded tomcat on port 8080...

    $ cd helloBoot
    $ ./gradlew bootRun

To test the app in a browser...

    http://localhost:8080/home